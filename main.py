import sys
from crawl import crawl_page
from report import print_report

def main():
    if len(sys.argv) != 2:
        exit(code=1)
    print(sys.argv[1])

    url_counts = crawl_page(sys.argv[1], None, {})
    print_report(url_counts)

if __name__ == "__main__":
    main()
