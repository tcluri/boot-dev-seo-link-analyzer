"""
crawl.py

Seo link analyser
https://boot.dev/project/59fbb2aa-7d67-4e88-bac8-42f49798a9f5/3b13416a-55a6-4b5a-93ba-faf5741d6a2f
"""
from lxml import html

def get_urls_from_string(page_content, base_url):
    page_content = page_content.text
    doc_tree = html.fromstring(page_content)
    doc_tree.make_links_absolute(base_url)
    links = []
    for elem in doc_tree.iter():
        if elem.tag == "a":
            # print(type(elem.get("href")))
            links.append(elem.get("href"))
    return links

from urllib.parse import urlparse

def normalize_url(url):
    parsed_url = urlparse(url)
    # print(parsed_url)
    url_from_parsed = parsed_url.netloc + parsed_url.path
    # print("Netloc + path: {}".format(url_from_parsed))
    url_from_parsed = url_from_parsed.lower()
    # print("Lowercased: {}".format(url_from_parsed))
    return url_from_parsed.strip("/")

import requests

def crawl_page(base_url, url, pages):
    current_url = url
    if not url:
        # Take the base_url as current url for the first iteration
        current_url = base_url
    norm_current_url = normalize_url(current_url)
    pages[norm_current_url] = pages.get(norm_current_url, 0)

    # Parsing the base_url and current_url
    parse_base_url = urlparse(base_url)
    parse_current_url = urlparse(current_url)

    # Current URL isn't on this site, and therefore isn't an internal link
    # Let's ignore it by setting it's entry in pages to None
    if parse_base_url.netloc != parse_current_url.netloc:
        # Host comparision base url and current url
        pages[norm_current_url] = None
    # If the entry in pages for the normalized url is None
    # let's return early
    if pages[norm_current_url] == None:
        return pages
    # If the entry in pages for the normalized url is greater than 0
    if pages[norm_current_url] > 0:
        pages[norm_current_url] += 1
        return pages
    # Make a request to the current_url
    fetch_webpage = requests.get(current_url)
    try:
        validate_url(fetch_webpage)
    except Exception as e:
        print(e)
        pages[norm_current_url] = None
        return pages
    # Increment the count in pages for normalized url
    pages[norm_current_url] += 1
    # Get all the urls in the html response and recursively crawl each one
    urls_in_webpage = get_urls_from_string(fetch_webpage, base_url)
    for each_url in urls_in_webpage:
       crawl_page(base_url, each_url, pages)
    return pages

def validate_url(resp):   # url
    if (resp.status_code != 200):
        raise Exception("Status code is not 200.")
    # if resp.headers['Content-Type'] != "text/html":
    if "text/html" not in resp.headers['Content-Type']:
        raise Exception("Content type is not a html text string")
