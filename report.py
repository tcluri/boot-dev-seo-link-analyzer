"""
Report python file that contains
1. remove_none_values function which remove keys with values None
2. sort_pages function which gives a sorted list containing (key,value) from a dictionary
   by values of the dictionary
3. print_report function which uses the above functions and prints them as a sentence
"""
def remove_none_values(input_dict):
    out_dict = {}
    for key in input_dict:
        if input_dict[key] == None:
            continue
        else:
            out_dict[key] = input_dict[key]
    return out_dict

def sort_pages(unsorted_dict):
    pages_list = [(key, unsorted_dict[key]) for key in unsorted_dict]
    pages_list.sort(reverse=True, key=lambda x:x[1])
    return pages_list

def print_report(pages):
    pages_clean_none = remove_none_values(pages)
    pages_sort = sort_pages(pages_clean_none)
    for each_page in pages_sort:
        print("Found {} internal links to {}".format(each_page[1], each_page[0]))
